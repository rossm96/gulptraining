// import the gulp node package
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    compass = require('gulp-compass'),
    plumber = require('gulp-plumber');

// Scripts Task
// Uglifies
gulp.task('scripts', function(){
	// load the files
	gulp.src('javascripts/*.js')

	.pipe(plumber())
	.pipe(uglify())
	.pipe(gulp.dest('build/js'))	
});

// Styles Task
gulp.task('styles', function() {
	gulp.src('sass/*.scss')

	.pipe(plumber())
	.pipe(compass({
		config_file: 'config.rb',
		css: 'stylesheets',
		sass: 'sass'//,
		//style: 'compressed'	
	}))
	.pipe(gulp.dest('stylesheets/'));
});


// Watch Task
// Watches JS
gulp.task('watch', function() {
	gulp.watch('javascripts/*.js', ['scripts']);
	gulp.watch('sass/**/**/*.scss', ['styles']);
});

gulp.task('default',   ['scripts', 'styles', 'watch']);


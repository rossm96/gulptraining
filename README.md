# README #

This repo is for learning gulp.

Checking out this project will involve a fully functioning front end project with a ready-to-go gulp task runner which will compile and minify javascript, and compass.

It will watch for any changes in javascript and css.

To get started clone the directory and simply cd into the directory via terminal and run:

    npm install

This will install the Gulp dependencies. Then to compile the project:

    gulp
